<?php
/**
 * Main class.
 */

namespace BbpressAnimafacResources;

use CivicrmApi\Tag;

/**
 * Class containing main actions and filters.
 */
class Main
{

    /**
     * Add scripts to head
     */
    public function addScripts()
    {
        wp_enqueue_script('bbpress-animafac-resources-suggest', plugins_url('js/suggest.js', __DIR__), ['jquery']);
    }
}
