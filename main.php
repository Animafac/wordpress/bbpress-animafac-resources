<?php
/*
Plugin Name: bbPress Animafac resources
Version: 0.1.0
Author: Animafac
Author URI: https://www.animafac.net/
Plugin URI: https://framagit.org/Animafac/bbpress-animafac-resources
Description: Suggest Animafac resources in bbPress
 */

use BbpressAnimafacResources\Main;

require_once __DIR__.'/vendor/autoload.php';

add_action('wp_enqueue_scripts', [Main::class, 'addScripts']);
