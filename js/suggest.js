/*global jQuery*/
/*jslint browser: true*/
var animafacSuggest = (function () {
    "use strict";
    var topicTitle;
    var suggestWrapper;

    function addResource(i, item) {
        suggestWrapper.find("ul").append("<li><a target='_blank' data-i='" + i + "' href='" + item.link + "'>" + item.title.rendered + "</a></li>");
    }

    function addResources(resources) {
        if (!suggestWrapper) {
            suggestWrapper = jQuery("<div id='animafac_suggest'>Ces ressources Animafac peuvent vous aider&nbsp;:<ul></ul></div>");
            topicTitle.after(suggestWrapper);
        }
        if (resources.length === 0) {
            suggestWrapper.hide();
        } else {
            suggestWrapper.show();
            suggestWrapper.find("ul").empty();
            jQuery.each(resources, addResource);
        }
    }

    function titleChange(e) {
        jQuery.get(
            "http://localhost/animafac/wp-json/wp/v2/incsub_wiki",
            {
                per_page: 3,
                search: e.target.value,
                orderby: "relevance"
            },
            addResources
        );
    }

    function init() {
        topicTitle = jQuery("#bbp_topic_title");
        topicTitle.change(titleChange);
    }

    return {
        init: init
    };
}());

jQuery(document).ready(animafacSuggest.init);
